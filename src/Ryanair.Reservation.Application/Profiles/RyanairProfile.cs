﻿using System;
using AutoMapper;
using Ryanair.Reservation.Application.Contracts;
using Ryanair.Reservation.Application.DTO;
using Ryanair.Reservation.Domain.Entities;
using Ryanair.Reservation.Domain.Validation;

namespace Ryanair.Reservation.Application.Profiles
{
    public class RyanairProfile : Profile
    {
        public RyanairProfile()
        {
            CreateMap<Flight, FlightResponse>().ReverseMap();
            CreateMap<Domain.Entities.Reservation, ReservationConfirmationResponse>().ReverseMap();
            CreateMap<Domain.Entities.Reservation, ReservationResponse>().ReverseMap();
            CreateMap<Domain.Entities.ReservationFlight, ReservationFlightDto>().ReverseMap();
            CreateMap<Domain.Entities.Passenger, PassengerResponse>().ReverseMap();

            CreateMap<DomainValidationException, DomainValidationExceptionResponse>().ReverseMap();
            CreateMap<System.Exception, ExceptionResponse>().ReverseMap();

        }
    }
}
