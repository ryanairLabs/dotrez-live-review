﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ryanair.Reservation.Domain.Validation;

namespace Ryanair.Reservation.Application.Contracts
{
    [Serializable]
    public class DomainValidationExceptionResponse : ExceptionResponse
    {
        [XmlElement]
        public List<DomainValidationMessage> ValidationMessages { get; set; }
    }
}
