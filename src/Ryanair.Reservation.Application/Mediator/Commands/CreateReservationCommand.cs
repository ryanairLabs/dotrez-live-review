﻿using System;
using MediatR;
using Ryanair.Reservation.Application.Contracts;
using Ryanair.Reservation.Domain.ValueObjects;

namespace Ryanair.Reservation.Application.Mediator.Commands
{
    public class CreateReservationCommand : ReservationData, IRequest<ReservationConfirmationResponse>
    {
    }
}
