﻿using System.Collections.Generic;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Ryanair.Reservation.Application.Contracts;
using Ryanair.Reservation.Application.Mediator.Commands;
using Ryanair.Reservation.Application.Mediator.Queries.Flight;
using Ryanair.Reservation.Application.Mediator.Queries.Reservation;

namespace Ryanair.Reservation.Application.Mediator
{
    public static class MediatorExtensions
    {
        public static IServiceCollection AddMediatorHandlers(this IServiceCollection services)
        {
            services.AddTransient<IRequestHandler<CreateReservationCommand, ReservationConfirmationResponse>, CreateReservationCommandHandler>();

            services.AddTransient<IRequestHandler<GetFlightsByParamQuery, List<FlightResponse>>, GetFlightsByParamQueryHandler>();
            services.AddTransient<IRequestHandler<GetReservationQuery, ReservationResponse>, GetReservationQueryHandler>();

            return services;
        }
    }
}
