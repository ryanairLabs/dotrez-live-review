﻿using System;
using MediatR;
using Ryanair.Reservation.Application.Contracts;

namespace Ryanair.Reservation.Application.Mediator.Queries.Reservation
{
    public class GetReservationQuery : IRequest<ReservationResponse>
    {
        public string ReservationNumber { get; set; }
    }
}
