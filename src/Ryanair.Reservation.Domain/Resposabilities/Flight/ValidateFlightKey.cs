﻿using System;
using System.Collections.Generic;
using Ryanair.Reservation.Domain.Interfaces;
using Ryanair.Reservation.Domain.Resources;
using Ryanair.Reservation.Domain.Validation;
using Ryanair.Reservation.Domain.ValueObjects;

namespace Ryanair.Reservation.Domain.Resposabilities.Flight
{

    public class ValidateFlightKey : IRulesValidation
    {
        protected readonly ReservationData _command;

        public IRulesValidation Next { get; set; }

        public ValidateFlightKey(ReservationData command)
        {
            _command = command;
        }

        /// <summary>
        /// Check if flight information is consistent
        /// </summary>
        /// <param name="messages">Messages.</param>
        public void Validate(List<DomainValidationMessage> messages)
        {

            foreach (var item in _command?.Flights)
            {
                if (string.IsNullOrEmpty(item.Key))
                {
                    messages.Add(new DomainValidationMessage { Level = ValidationLevel.Error, Message = string.Format(Language.FlightNullEmpty, item.Key), Property = nameof(item.Key) });
                }
            }

            //go to next validation
            if (this.Next != null)
            {
                this.Next.Validate(messages);
            }
        }
    }


}
