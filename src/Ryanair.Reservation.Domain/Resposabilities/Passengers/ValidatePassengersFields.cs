﻿using System.Collections.Generic;
using Ryanair.Reservation.Domain.Constants;
using Ryanair.Reservation.Domain.Interfaces;
using Ryanair.Reservation.Domain.Resources;
using Ryanair.Reservation.Domain.Validation;
using Ryanair.Reservation.Domain.ValueObjects;

namespace Ryanair.Reservation.Domain.Resposabilities.Passengers
{
    public class ValidatePassengersFields : IRulesValidation
    {
        protected readonly PassengerData _command;
        public IRulesValidation Next { get; set; }

        public ValidatePassengersFields(PassengerData command)
        {
            _command = command;
        }

        /// <summary>
        /// Check if Passenger information is valid
        /// </summary>
        /// <param name="messages">Messages.</param>
        public void Validate(List<DomainValidationMessage> messages)
        {
            if (string.IsNullOrEmpty(_command.Name))
            {
                messages.Add(new DomainValidationMessage { Level = ValidationLevel.Error, Message = Language.PassengerNameMandatory, Property = nameof(_command.Name) });
            }

            if (string.IsNullOrEmpty(_command.Seat))
            {
                messages.Add(new DomainValidationMessage { Level = ValidationLevel.Error, Message = Language.SeatNumberMandatory, Property = nameof(_command.Seat) });
            }

            if (_command.Bags > RyanairConstants.MAX_BAGS_PASSENGER)
            {
                messages.Add(new DomainValidationMessage { Level = ValidationLevel.Error, Message = string.Format(Language.MaxBagsPerUser, _command.Name), Property = nameof(_command.Bags) });
            }

            if (this.Next != null)
            {
                this.Next.Validate(messages);
            }
        }
    }
}
