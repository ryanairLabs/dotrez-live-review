﻿using System;
using System.Collections.Generic;
using Ryanair.Reservation.Domain.Validation;

namespace Ryanair.Reservation.Domain.Interfaces
{
    public interface IRulesValidation
    {
        void Validate(List<DomainValidationMessage> messages);
        IRulesValidation Next { get; set; }
    }
}
